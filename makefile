.cpp.o:
	@$(CC) -g -c $<

CC = g++
SOURCE = Buffer.cpp Console.cpp FileDest.cpp Object.cpp ObjectManager.cpp 
OBJS = ${SOURCE:.cpp=.o}

all: testRenderer
	@echo "Build Success\n\nOutput is... \n\n"
	@./testRenderer

.PHONY: clean 

clean:
	- @rm *.*o
	- @rm testRenderer

testRenderer: $(OBJS) RenderFactory.o Renderer.o testRenderer.o
	@$(CC) -o $@ $^ -lpthread -ldl

RenderFactory.o: RenderFactory.cpp RenderFactory.h libFile.so libConsole.so
	@$(CC) -g -c $< 

Renderer.o: Renderer.cpp Renderer.h
	@$(CC) -g -c $< 

testRenderer.o: testRenderer.cpp Renderer.o RenderFactory.o
	@$(CC) -g -c $< 

libFile.so: FileDest.cpp FileDest.h
	@$(CC) -shared -fPIC -o libFile.so $<

libConsole.so: Console.cpp Console.h
	@$(CC) -shared -fPIC -o libConsole.so $<
