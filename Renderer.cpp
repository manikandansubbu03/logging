#include <dlfcn.h>
#include<iostream>
#include <string>
#include<cstdlib>
#include<pthread.h>
#include<vector>
#include<unistd.h>
using namespace std;

#include "ObjectManager.h"
#include "Object.h"
#include "global.h"
#include "Buffer.h"
#include "Destination.h"
#include "FileDest.h"
#include "Console.h"
#include "RenderFactory.h"
#include "Renderer.h"

Renderer* Renderer::r=NULL; 
bool Renderer::signal=false;
bool Renderer::go=false;

Renderer* Renderer::CreateInstance()
{
	if(r==NULL)
		r=new Renderer();
	return r;
}

Renderer::Renderer()
{
	Renderer::signal=0;
//	cout<<"Inside Constructor"<<endl;
	int rc = pthread_create(&thread, NULL,renderMsg,this);
}

vector<Buffer*>* Renderer::getBuffer()
{
	return &(this->buffer);
}

void Renderer::setRenderFactory(RenderFactory* renderFactory)
{
	this->renderFactory=renderFactory;
}

void Renderer::setDestination(Destination* destination)
{
	this->destination=destination;
}

Destination* Renderer::getDestination()
{
	return this->destination;
}

RenderFactory* Renderer::getRenderFactory()
{
	return this->renderFactory;
}

bool Renderer::getSignal()
{
	return Renderer::signal;
}

/*void* Renderer::renderMsg(void* s)
{
	return ((Renderer*)s)->renderMsgHelper(s);
}
*/
void* Renderer::renderMsg(void* s)
{
	Renderer* t =static_cast<Renderer*>(s);
//	cout<<"Inside Thread"<<endl;
	t->setRenderFactory(RenderFactory::CreateObject());
//	cout<<"RenderFactory"<<endl<<t->getRenderFactory();
	vector<Buffer*> *temp= t->getBuffer();
	while(true)
	{	
		
//		cout<<"run"<<endl;
		if(t->getSignal()==1 && temp->size() == 0)
		{
			temp->clear();
			Renderer::go=true;
			//cout<<"exit"<<endl;
			pthread_exit(NULL);
		}
	//	cout<<"Size : " <<temp->size()<<endl;
		while(temp->size()==0)
		{
			cout<<"Buffer Empty";
		}
	//	if(t->getBuffer().size()!=0)
	//	{
			//cout<<"Buffer Not Empty"<<endl;
			for(unsigned int i=0;i<temp->size();)
			{
				t->setDestination(t->getRenderFactory()->getInstance((*temp)[i]->getType()));
//				cout<<t->getDestination()<<endl;
				if(t->getDestination()->displayMsg((*temp)[i]->getDestinationDetails(),(*temp)[i]->getMessage()))
				{
					//cout<<"Displayed Msg"<<endl;
					//t->getBuffer().pop_back();

				
//					t->getBuffer().erase(t->getBuffer().begin());
					temp->erase(temp->begin());
					delete t->getDestination();
					continue;
				}
				delete t->getDestination();
				i++;
			}
	//	}
	}
}

string Renderer::processMsg(string destination,string destinationDetails,string message)
{
	Buffer* buf1=new Buffer();
	buf1->setType(destination);
	buf1->setDestinationDetails(destinationDetails);
	buf1->setMessage(message);
	buffer.push_back(buf1);
//	cout<<buf1->getType()<<endl<<buf1->getDestinationDetails()<<endl<<buf1->getMessage();
	return message;
}

void Renderer::close()
{

	int r = pthread_join(thread, NULL);
	if(r)
	{
		cout<<"unable to join"<<endl;
	}
	Renderer::signal=0;

}
Renderer::~Renderer()
{
	/*while(Renderer::go==false)
	{}*/
	Renderer::signal = 1;	
	//if(Renderer::signal!=0)
	//{
		this->close();
	//}
	//cout<<"Destruct"<<endl;
}
