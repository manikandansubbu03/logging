#pragma once
class Renderer : public Object
{
	static Renderer* r;
	pthread_t thread;
	vector<Buffer*> buffer;
	Destination* destination;
	static bool signal;
	static bool go;
	RenderFactory *renderFactory;	
	Renderer();
	~Renderer();
	
	public:
		static Renderer* CreateInstance();				
		
		static void * renderMsg(void*);
		void setDestination(Destination*);
		void setRenderFactory(RenderFactory*);
		void* renderMsgHelper(void*);
		vector<Buffer*>* getBuffer();
		
		Destination* getDestination();
		RenderFactory* getRenderFactory();
		bool getSignal();
			void close();
		string processMsg(string,string,string);
//		Buffer* getBuffer();
		
		friend class ObjectManager;
		
};
