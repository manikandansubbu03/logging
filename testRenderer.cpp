#include <dlfcn.h>
#include<iostream>
#include <string>
#include<cstdlib>
#include<pthread.h>
#include<vector>
#include<unistd.h>
using namespace std;
//LD_LIBRARY_PATH=.

#include "ObjectManager.h"
#include "global.h"
#include "Object.h"
#include "Destination.h"
#include "Buffer.h"
#include "FileDest.h"
#include "Console.h"
#include "RenderFactory.h"
#include "Renderer.h"

int main()
{
	Renderer *r = Renderer::CreateInstance();
	r->processMsg("File","/home/sa/c++_programs/log/loggerdata/log.txt","File Logging");
	r->processMsg("File","/home/sa/c++_programs/log/loggerdata/log.txt","File Logging1");
	r->processMsg("File","/home/sa/c++_programs/log/loggerdata/log.txt","File Logging2");
	r->processMsg("Console","/home/sa/c++_programs/log/loggerdata","Console Logging");
	r->processMsg("Console","/home/sa/c++_programs/log/loggerdata","Console Logging");
//	delete r;
//	r->~Renderer();
//	sleep(10);
	return 0;
}
