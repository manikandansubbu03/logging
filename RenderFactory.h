//RenderFactory.h

#pragma once

typedef Destination *(*FPCREATE)();
class RenderFactory: public Object
{

	static RenderFactory* rf;
	//int size;

	RenderFactory();
	~RenderFactory();
public:
	static RenderFactory* CreateObject();
	
	Destination * getInstance(string str);
	

			friend class ObjectManager;
};
