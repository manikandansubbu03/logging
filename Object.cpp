#include <iostream>
#include <vector>
using namespace std;

#include "ObjectManager.h"
#include "Object.h"
#include "global.h"

#define ADD_OBJECT(obj) \
	g_ObjectManager.addObject(obj);

Object::Object()
{
	//cout<<"Object Constructor"<<endl;
	ADD_OBJECT(this);
}
Object::~Object()
{
	//cout<<"Object Destructor"<<endl;
}
