#include<string>
using namespace std;

#include "Buffer.h"

void Buffer::setType(string type)
{
	this->type=type;
}

string Buffer::getType()
{
	return type;
}

void Buffer::setDestinationDetails(string destinationDetails)
{
	this->destinationDetails=destinationDetails;
}

string Buffer::getDestinationDetails()
{
	return destinationDetails;
}

void Buffer::setMessage(string message)
{
	this->message=message;
}

string Buffer::getMessage()
{
	return message;
}
