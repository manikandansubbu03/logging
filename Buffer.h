#pragma once
class Buffer
{
	string type;
	string destinationDetails;
	string message;

	public:
		void setType(string);
		string getType();
		
		void setDestinationDetails(string);
		string getDestinationDetails();

		void setMessage(string);
		string getMessage();
};
