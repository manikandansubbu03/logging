#include <dlfcn.h>
#include <cstdio>
#include <iostream>
#include<string>
#include <vector>

using namespace std;

#include "ObjectManager.h"
#include "Object.h"
#include "global.h"
#include "Destination.h"
#include "FileDest.h"
#include "Console.h"
#include "RenderFactory.h"

void *handle=NULL;
RenderFactory* RenderFactory::rf=NULL;

RenderFactory::RenderFactory()
{
	//cout<<"Inside RenderFactory Constructor"<<endl;
}

RenderFactory::~RenderFactory()
{

}
RenderFactory* RenderFactory::CreateObject()
{
	if(rf==NULL)
	{
		//cout<<"Creating Object For Render Factory"<<endl;
		rf = new RenderFactory();
		//cout<<"Done"<<endl;
	}
		return rf;
}

Destination* RenderFactory::getInstance(string str)
{
/*	Destination* d=NULL;
	if(id==0)
	{
		if(dest[id]==NULL)
			dest[id]=new FileDest();
		d=dest[id];
	}
	else if(id==1)
	{
		if(dest[id]==NULL)
			dest[id]=new Console();
		d=dest[id];
	}
	return d;
*/
/*
	if(str.compare("File")==0)
	{
//		cout<<"File"<<endl;
		return new FileDest();
	}
	else if(str.compare("Console")==0)
	{
//		cout<<"Console"<<endl;
		return new Console();
	}
*/

		Destination *pX;
		string s= "lib"+str+".so";
		handle = dlopen(s.c_str(), RTLD_LAZY);
		if (handle == NULL)
		{
			printf("Failed opening the library\n");
			return 	NULL;
		}
		string func = str+"Control";
		FPCREATE Create = (FPCREATE)dlsym(handle, func.c_str());
		if (Create == NULL)
		{
			dlclose(handle);
			printf("Failed getting the address of CreateInstance\n");
			return NULL;
		}
		pX = Create();
		//dlclose(handle);	
		return pX;	
}




