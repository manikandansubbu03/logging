#pragma once
class Destination
{
	public:
		virtual bool isAlive(string)=0;
		virtual bool displayMsg(string,string)=0;
};
